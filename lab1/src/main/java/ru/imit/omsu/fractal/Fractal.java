package ru.imit.omsu.fractal;

import javax.swing.*;
import java.awt.*;

// Фрактал - дерево Пифагора
public class Fractal extends JFrame{
    private final int width = 1000;
    private final int height = 1000;

    public Fractal(){
        this.setTitle("Fractal");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setSize(width, height);
        this.setVisible(true);
        this.getContentPane().setBackground(new Color(255, 255, 255));
    }

    public static void main(String[] args) {
        Fractal fractal = new Fractal();
        fractal.setVisible(true);
        fractal.setBackground(Color.black);
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.green);
        drawFractalTree((Graphics2D) g, width / 2, height , -90, 20, ( (double) (width + height) / 2) / 4);
    }

    public void drawFractalTree(Graphics2D g, int x1, int y1, double angle, int depth, double size) {
        if (depth > 0) {
            int x2 = x1 + (int) (Math.cos(Math.toRadians(angle)) * size);
            int y2 = y1 + (int) (Math.sin(Math.toRadians(angle)) * size);

            g.drawLine(x1, y1, x2, y2);

            drawFractalTree(g, x2, y2, angle + 90, depth - 1, size * 0.67);
            drawFractalTree(g, x2, y2, angle - 90, depth - 1, size * 0.67);
        }
    }

}